# CV04
State machine with 5 states (ON, OFF, AM/FM, MP3, HandsFree).

- each state own thread
- message passing through eventflags
- semaphore accepts one entry into critical section (display line 0).
- [DEMO](https://www.youtube.com/watch?v=76YfcanFbfA)
#include "mbed.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include "rtos.h"
#include <cstdint>
#include <cstdio>
#include <exception>
#include <iterator>
#include <string>

//DISPLAY IS 480×272
#define FIRST_BUTTON_POSITION 50
#define BUTTON_WIDTH 125
#define BUTTON_HEIGHT 50
#define ROW_OFFSET 80
#define COLUMN_OFFSET 150
#define INTERRUPT_FLAG (1UL << 0)
#define INTERRUPTED_FLAG (1UL << 1)
#define STATE_CHANGING (1UL << 6)
#define START_MP3 (1UL << 2)
#define START_AM_FM (1UL << 3)
#define START_HANDS_FREE (1UL << 4)
#define START_RADIO_OFF (1UL << 5)
#define START_RADIO_ONLINE (1UL << 7)

#define DEBUG_LINE 2

typedef enum tState 
{ 
  STATE_MP3,
  STATE_AM_FM,
  STATE_HANDS_FREE,
  STATE_RADIO_OFF,
  STATE_RADIO_ON
} tState;
tState state; 
tState previous_state;

struct threadProperties {
  int ID;
  unsigned long int FLAG;
  string statusText; //string is cursed, void pointer is cursed. Monkey time -> string on ID, glorious.
};
#pragma region FORWARD_DECLARATIONS
void initDisplay();
void initTS();
void setupUI();
void getTouchCoords(uint16_t &x, uint16_t &y);
void isButtonClicked(uint16_t x, uint16_t y);
void stateMachine(tState state);
void setupThreads();
void stateThread(struct threadProperties *t);
void onlineIcon(bool radioStarted);
#pragma endregion

TS_StateTypeDef TS_State;
uint16_t x, y;
Semaphore one_slot(1);
volatile bool radioStarted = false;
volatile bool threadIsRunning = false;
EventFlags thread_comm;
EventFlags state_comm;
//Thread threadState; //can only be started once..
Thread MP3_thread;
Thread AM_FM_thread;
Thread Hands_free_thread;
Thread Radio_OFFLINE_thread;
Thread Radio_ONLINE_thread;

int main()
{
    initDisplay();
    initTS();
    setupUI();
    
    threadProperties threadProperties1 =
    { .ID = 1, .FLAG = START_MP3, .statusText="MP3"};
    threadProperties threadProperties2 =
    { .ID = 2, .FLAG = START_AM_FM, .statusText="AM/FM"};
    threadProperties threadProperties3 =
    { .ID = 3, .FLAG = START_HANDS_FREE, .statusText="HANDS_FREE"};
    threadProperties threadProperties4 =
    { .ID = 4, .FLAG = START_RADIO_OFF, .statusText="RADIO OFF"};
    threadProperties threadProperties5 =
    { .ID = 5, .FLAG = START_RADIO_ONLINE, .statusText="RADIO ON"};
    
    MP3_thread.start(callback(stateThread, &threadProperties1));
    AM_FM_thread.start(callback(stateThread, &threadProperties2));
    Hands_free_thread.start(callback(stateThread, &threadProperties3));
    Radio_OFFLINE_thread.start(callback(stateThread, &threadProperties4));
    Radio_ONLINE_thread.start(callback(stateThread, &threadProperties5));
    previous_state = STATE_RADIO_ON; //anything other than init state.
    state = STATE_RADIO_OFF; //init state
    state_comm.set(STATE_CHANGING); //init flag
    while (true) {
        onlineIcon(radioStarted);
        getTouchCoords(x,y);
        isButtonClicked(x, y);
        if(state_comm.get()==STATE_CHANGING)
        {
            state_comm.clear(STATE_CHANGING);
            stateMachine(state);
        }
        HAL_Delay(200);
    }
}
void stateThread(struct threadProperties *t)
{
start:
  thread_comm.wait_any(t->FLAG);
  one_slot.acquire();
  const uint8_t* p = reinterpret_cast<const uint8_t*>(t->statusText.c_str());
  uint8_t text[30];
  sprintf((char *)text, "THREAD ID:%d OP:%s", t->ID, p);
  BSP_LCD_ClearStringLine(0);
  threadIsRunning = true;
  while (1) {
    BSP_LCD_DisplayStringAt(0, LINE(0), (uint8_t *)&text, LEFT_MODE);
    BSP_LCD_DisplayStringAt(0, LINE(DEBUG_LINE), (uint8_t *)"tr run",
                            RIGHT_MODE);

    if (thread_comm.get() == INTERRUPT_FLAG) {
      threadIsRunning = false;
      BSP_LCD_DisplayStringAt(0, LINE(DEBUG_LINE), (uint8_t *)"tr int",
                              RIGHT_MODE);
      thread_comm.clear(INTERRUPT_FLAG);
      one_slot.release();
      thread_comm.set(INTERRUPTED_FLAG);
      goto start;
    }
    }
    
}
void stateMachine(tState state)
{
    again:
    if(threadIsRunning)
    {
        thread_comm.set(INTERRUPT_FLAG);
        thread_comm.wait_any(INTERRUPTED_FLAG);
        // threadState.terminate();
        // threadState.join();
        goto again;
    }
    switch (state) {
        case STATE_MP3:
        {
            if(threadIsRunning==false)
            {
              thread_comm.set(START_MP3);
            }
        }
        case STATE_AM_FM:
        {
            if(threadIsRunning==false)
            {
              thread_comm.set(START_AM_FM);
            }
        }
        case STATE_HANDS_FREE:
        {
            if(threadIsRunning==false)
            {
              thread_comm.set(START_HANDS_FREE);
            }
        }
        case STATE_RADIO_OFF:
        {
            if(threadIsRunning==false)
            {
              thread_comm.set(START_RADIO_OFF);
            }
        }
        case STATE_RADIO_ON:
        {
            if(threadIsRunning==false)
            {
              thread_comm.set(START_RADIO_ONLINE);
            }
        }
    }  
}
void setupThreads()
{
    //??????????????????????????????????? THIS METHOD FOR SOME REASON dasdasd UP ID's?
    threadProperties threadProperties1 =
    { .ID = 1, .FLAG = START_MP3};
    threadProperties threadProperties2 =
    { .ID = 2, .FLAG = START_AM_FM};
    threadProperties threadProperties3 =
    { .ID = 3, .FLAG = START_HANDS_FREE};
    threadProperties threadProperties4 =
    { .ID = 4, .FLAG = START_RADIO_OFF};
    
    MP3_thread.start(callback(stateThread, &threadProperties1));
    AM_FM_thread.start(callback(stateThread, &threadProperties2));
    Hands_free_thread.start(callback(stateThread, &threadProperties3));
    Radio_OFFLINE_thread.start(callback(stateThread, &threadProperties4));
}
#pragma region DISPLAY
void onlineIcon(bool radioStarted)
{
    if(radioStarted)
    {
        BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
    }
    else {
        BSP_LCD_SetTextColor(LCD_COLOR_RED);
    }
    BSP_LCD_FillCircle(10, 50, 5);
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
}
void isButtonClicked(uint16_t x, uint16_t y)
{
    bool hasStateChanged = (previous_state!=state) ? true : false;
    //row 0 col 0 ON
    if(x>=FIRST_BUTTON_POSITION && x<=FIRST_BUTTON_POSITION+BUTTON_WIDTH && y>=FIRST_BUTTON_POSITION && y<=FIRST_BUTTON_POSITION+BUTTON_HEIGHT)
    {
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *)"1", RIGHT_MODE);
        if(!radioStarted)
        {
            radioStarted = true;
            state = previous_state;
            previous_state = STATE_RADIO_OFF;
            state_comm.set(STATE_CHANGING);
        }
    }
    //row 1 col 0 OFF
    if(x>=FIRST_BUTTON_POSITION && x<=FIRST_BUTTON_POSITION+BUTTON_WIDTH && y>=FIRST_BUTTON_POSITION+ROW_OFFSET*1 && y<=FIRST_BUTTON_POSITION+ROW_OFFSET*1+BUTTON_HEIGHT)
    {
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *)"2", RIGHT_MODE);
        if(radioStarted && hasStateChanged)
        {   
            previous_state = state;
            radioStarted = false;
            state = STATE_RADIO_OFF;
            state_comm.set(STATE_CHANGING);
        }
    }
    //row 2 col 0 AM/FM
    if(x>=FIRST_BUTTON_POSITION && x<=FIRST_BUTTON_POSITION+BUTTON_WIDTH && y>=FIRST_BUTTON_POSITION+ROW_OFFSET*2 && y<=FIRST_BUTTON_POSITION+ROW_OFFSET*2+BUTTON_HEIGHT)
    {
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *)"3", RIGHT_MODE);
        if(radioStarted && hasStateChanged)
        {
            previous_state = state;
            state = STATE_AM_FM;
            state_comm.set(STATE_CHANGING);
        }
    }
    //row 0 col 1 mp3
    if(x>=FIRST_BUTTON_POSITION+COLUMN_OFFSET && x<=FIRST_BUTTON_POSITION+COLUMN_OFFSET+BUTTON_WIDTH && y>=FIRST_BUTTON_POSITION && y<=FIRST_BUTTON_POSITION+BUTTON_HEIGHT)
    {
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *)"4", RIGHT_MODE);
        if(radioStarted && hasStateChanged)
        {
            previous_state = state;
            state= STATE_MP3;
            state_comm.set(STATE_CHANGING);
        }
    }
    //row 1 col 1 call incoming
    if(x>=FIRST_BUTTON_POSITION+COLUMN_OFFSET && x<=FIRST_BUTTON_POSITION+COLUMN_OFFSET+BUTTON_WIDTH && y>=FIRST_BUTTON_POSITION+ROW_OFFSET*1 && y<=FIRST_BUTTON_POSITION+ROW_OFFSET*1+BUTTON_HEIGHT)
    {
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *)"5", RIGHT_MODE);
        if(radioStarted && hasStateChanged)
        {
            previous_state = state;
            state = STATE_HANDS_FREE;
            state_comm.set(STATE_CHANGING);
        }
    }
    //row 2 col 1 call ougoing
    if(x>=FIRST_BUTTON_POSITION+COLUMN_OFFSET && x<=FIRST_BUTTON_POSITION+COLUMN_OFFSET+BUTTON_WIDTH && y>=FIRST_BUTTON_POSITION+ROW_OFFSET*2 && y<=FIRST_BUTTON_POSITION+ROW_OFFSET*2+BUTTON_HEIGHT)
    {
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *)"6", RIGHT_MODE);
        if(radioStarted && hasStateChanged)
        {
            previous_state = state;
            state = STATE_HANDS_FREE;
            state_comm.set(STATE_CHANGING);
        }
    }
}
void getTouchCoords(uint16_t &x, uint16_t &y)
{
        uint8_t coords_txt_x[6];
        uint8_t coords_txt_y[6];
        BSP_TS_GetState(&TS_State);
        if (TS_State.touchDetected == 0) {
            x=0;
            y=0;
        }
        else {
            x= TS_State.touchX[0];
            y = TS_State.touchY[0];
        }
        sprintf((char*)coords_txt_x, "x:%d",x);
        sprintf((char*)coords_txt_y, "y:%d",y);
        BSP_LCD_DisplayStringAt(0, LINE(9), (uint8_t *)&coords_txt_x, RIGHT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(10), (uint8_t *)&coords_txt_y, RIGHT_MODE);
}
/**
Doesn't work for some reason, gg. Nevermind, horsecrap.
*/
void addButton(uint16_t pos_x, uint16_t pos_y, string text="", bool active=false, int width=BUTTON_WIDTH, int height=BUTTON_HEIGHT)
{
    const char *ptr_text = text.c_str();
    //BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE); //WHY THE duck THIS DOES DEFINE BACKGROUND?
    BSP_LCD_FillRect(pos_x, pos_y, BUTTON_WIDTH, BUTTON_HEIGHT);
    if(text!="")
    {
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_DisplayStringAt(pos_x+2, pos_y+BUTTON_HEIGHT/2, (uint8_t *)ptr_text, LEFT_MODE);
    }

}
void setupUI()
{
    BSP_LCD_Clear(LCD_COLOR_BLACK);
    BSP_LCD_SetFont(&LCD_DEFAULT_FONT);
    BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);

    //COLUMN 0
    //Turn radio on - 0.row
    addButton(FIRST_BUTTON_POSITION, FIRST_BUTTON_POSITION, "ON");
    //Turn radio off - 1.row
    addButton(FIRST_BUTTON_POSITION, FIRST_BUTTON_POSITION+ROW_OFFSET*1, "OFF");
    //Activate FM/AM - 2.row
    addButton(FIRST_BUTTON_POSITION, FIRST_BUTTON_POSITION+ROW_OFFSET*2, "AM/FM");
    //COLUMN 1 
    //Activate MP3
    addButton(FIRST_BUTTON_POSITION+COLUMN_OFFSET, FIRST_BUTTON_POSITION, "MP3");
    //INCOMING CALL
    addButton(FIRST_BUTTON_POSITION+COLUMN_OFFSET, FIRST_BUTTON_POSITION+ROW_OFFSET*1, "Call <-");
    //OUTGOING CALL
    addButton(FIRST_BUTTON_POSITION+COLUMN_OFFSET, FIRST_BUTTON_POSITION+ROW_OFFSET*2, "Call ->");
    //STATUS BAR
    //BSP_LCD_DisplayStringAt(0, LINE(0), (uint8_t *)"UNACTIVE", CENTER_MODE);
}
void initDisplay()
{
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
}
void initTS()
{
    uint8_t status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    if (status != TS_OK) {
        BSP_LCD_Clear(LCD_COLOR_RED);
        BSP_LCD_SetBackColor(LCD_COLOR_RED);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT FAIL", CENTER_MODE);
    } else {
        BSP_LCD_Clear(LCD_COLOR_GREEN);
        BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT OK", CENTER_MODE);
    }
    HAL_Delay(1000);
    BSP_LCD_Clear(LCD_COLOR_BLACK);
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
}
#pragma endregion

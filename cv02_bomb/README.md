# CV02
tick tock until boom

- default interval is 10 seconds
- add/subtract time is 5 seconds
- continuous bomb after expiracy the app resets due to watchdog
- counting is done on another thread, this thread also "kicks" the watchdog that is set on 1s.
- if time<0 thread stops and henceforth watchdog is not kicked -> reset
- [DEMO](https://www.youtube.com/watch?v=8eOwv0grehs)
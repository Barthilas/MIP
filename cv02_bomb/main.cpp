#include "mbed.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include <cstdio>
#include <string>


//DISPLAY IS 480×272
//VARIABLES
#define TOUCH_LINE_XY  10 //displays the stringline of current x,y coords
#define CIRCLE_Y    180
#define CIRCLE_LEFT_X   60
#define CIRCLE_RIGHT_X  425
#define CIRCLE_DIAMETER 40
#define TOUCH_SENSITIVITY 30 //how accurate your click needs to be

#define TIME_ADD_DECAY 5
#define START    10 //2 min

TS_StateTypeDef TS_State;
volatile int TIME = START;
volatile bool timeUp = false;

//FORWARD DECLARATIONS
void initDisplay();
void setupMenu();
void initTS();
void registerTouch();
bool inRange(unsigned low, unsigned high, unsigned x);
void displayTime();
void addTime();
void decayTime();
string timeToString(int time);
void startTimerThreaded();

int main()
{
    initDisplay();
    initTS();
    Thread thread_clock;
    thread_clock.start(startTimerThreaded);
    //ACCEPTS MS, START TIMER ON 1s and refresh(kick) periodically in new thread, if the kick does not happen the bomb has indeed exploded.
    Watchdog::get_instance().start(1000);
    while (!timeUp) {
        setupMenu();
        registerTouch();
        HAL_Delay(200);
    }
}
void startTimerThreaded() {
  while (TIME > 0) {
    TIME = TIME - 1;
    Watchdog::get_instance().kick();
    ThisThread::sleep_for(1s);
  }
  //BOOM SCREEN -> method??
  timeUp=true;
  BSP_LCD_Clear(LCD_COLOR_YELLOW);
}
void addTime()
{   Watchdog::get_instance().stop();
    TIME = TIME + TIME_ADD_DECAY;
}
void decayTime()
{
    TIME = TIME - TIME_ADD_DECAY;
    if(TIME<0)
    {
        TIME = 0;
    }

}
#pragma region DISPLAY
void registerTouch()
{
        uint16_t x, y;
        uint8_t text[30];
        //just in case...
        BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_TS_GetState(&TS_State);
        if (TS_State.touchDetected == 0) {
            BSP_LCD_ClearStringLine(TOUCH_LINE_XY);
        }
        else {
            x= TS_State.touchX[0];
            y = TS_State.touchY[0];
            sprintf((char*)text, "Touch: x=%d y=%d    ", x, y);
            BSP_LCD_DisplayStringAt(0, LINE(TOUCH_LINE_XY), (uint8_t *)&text, LEFT_MODE);
        }
        //PLUS BUTTON
        if(x>=CIRCLE_LEFT_X-TOUCH_SENSITIVITY && x<=CIRCLE_LEFT_X+TOUCH_SENSITIVITY && y>=CIRCLE_Y-TOUCH_SENSITIVITY && y<=CIRCLE_Y+TOUCH_SENSITIVITY)
        {
            addTime();
        }
        //MINUS BUTTON
        if(x>=CIRCLE_RIGHT_X-TOUCH_SENSITIVITY && x<=CIRCLE_RIGHT_X+TOUCH_SENSITIVITY && y>=CIRCLE_Y-TOUCH_SENSITIVITY && y<=CIRCLE_Y+TOUCH_SENSITIVITY)
        {
            decayTime();
        }

    
}
void initDisplay()
{
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
}
void initTS()
{
    uint8_t status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    if (status != TS_OK) {
        BSP_LCD_Clear(LCD_COLOR_RED);
        BSP_LCD_SetBackColor(LCD_COLOR_RED);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT FAIL", CENTER_MODE);
    } else {
        BSP_LCD_Clear(LCD_COLOR_GREEN);
        BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT OK", CENTER_MODE);
    }
    HAL_Delay(1000);
}

void setupMenu()
{
        BSP_LCD_Clear(LCD_COLOR_BLACK);
        BSP_LCD_SetFont(&LCD_DEFAULT_FONT);
        BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);

        BSP_LCD_DisplayStringAt(0, LINE(0), (uint8_t *)"HIROSHIMA", CENTER_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(1), (uint8_t *)"TIME LEFT:", CENTER_MODE);
        //left + button
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_FillCircle(CIRCLE_LEFT_X, CIRCLE_Y, CIRCLE_DIAMETER);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_DisplayStringAt(CIRCLE_LEFT_X-10, CIRCLE_Y-10, (uint8_t *)"+", LEFT_MODE);
        //right - button
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_FillCircle(CIRCLE_RIGHT_X, CIRCLE_Y, CIRCLE_DIAMETER);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_DisplayStringAt(CIRCLE_RIGHT_X-8, CIRCLE_Y-10, (uint8_t *)"-", LEFT_MODE);

        //display time
        displayTime();
}
void displayTime()
{
    int minutes, seconds;
    minutes = TIME / 60;
    seconds = TIME % 60;
    uint8_t time[10];
    sprintf((char*) time, "%d:%d", minutes, seconds);
    if (seconds < 10)
    {
        sprintf((char*) time, "%d:0%d", minutes, seconds);
    }
    BSP_LCD_SetTextColor(LCD_COLOR_RED);
    BSP_LCD_SetFont(&Font24);
    BSP_LCD_DisplayStringAt(0, LINE(2), (uint8_t *)time, CENTER_MODE);
}
bool inRange(unsigned low, unsigned high, unsigned x)
{
return (low <= x && x <= high);
}
#pragma endregion

# CV03
4 threads fight over the control of massive display in one hell of an epic battle. Semaphore, EventFlags example.

- 4 threads hard coded
- Threads take control over display for THREAD_SLEEP.
- Semaphore accepts one entry into critical section.
- Thread comms is solved by EventFlags.
- [DEMO](https://www.youtube.com/watch?v=AOH0lO2ezoE)
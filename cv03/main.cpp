/*
 * Copyright (c) 2020 Arm Limited and affiliates.
 * SPDX-License-Identifier: Apache-2.0
 */
#include "mbed.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include "rtos.h"
#include <cstdio>
#include <string>

#define THREADS_TO_CREATE 4
#define THREAD_SLEEP 1s
#define THREAD_TIMEOUT 200
#define STRING_LINE 5
#define SAMPLE_FLAG1 (1UL << 0)
#define SAMPLE_FLAG2 (1UL << 1)
#define SAMPLE_FLAG3 (1UL << 2)
#define SAMPLE_FLAG4 (1UL << 3)
#define THREAD_DONE_FLAG (1UL << 4)

TS_StateTypeDef TS_State;
EventFlags event_flags;
EventFlags thread_flags;
volatile int touched = 0;
volatile bool threadOperationRunning = false;
Semaphore one_slot(1);
Thread t1;
Thread t2;
Thread t3;
Thread t4;
//Thread threads[THREADS_TO_CREATE];

struct threadProperties {
  int ID;
  unsigned long int FLAG;
};

void test_thread(struct threadProperties *t)
{
    while(true)
    {
        event_flags.wait_any(t->FLAG);
        one_slot.acquire();
        uint8_t text[30];
        threadOperationRunning = true;
        sprintf((char*)text, "THREAD %d", (const char *)t->ID);
        BSP_LCD_DisplayStringAt(0, LINE(STRING_LINE+1), (uint8_t *)&text, CENTER_MODE);
        ThisThread::sleep_for(THREAD_SLEEP);
        threadOperationRunning = false;
        thread_flags.set(THREAD_DONE_FLAG);
        one_slot.release();
        
    }
    
}
void registerTouch()
{
        uint16_t x, y;
        uint8_t text[30];
        //just in case...
        BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_TS_GetState(&TS_State);
        if (TS_State.touchDetected>0) {
            HAL_Delay(200); //protect against multiple ticks
            unsigned int flag = 1UL << touched;
            touched++;
            if(touched>=THREADS_TO_CREATE)
            {
                touched = 0;
            }
            event_flags.set(flag);
        }
}
void initDisplay()
{
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
}
void initTS()
{
    uint8_t status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    if (status != TS_OK) {
        BSP_LCD_Clear(LCD_COLOR_RED);
        BSP_LCD_SetBackColor(LCD_COLOR_RED);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT FAIL", CENTER_MODE);
    } else {
        BSP_LCD_Clear(LCD_COLOR_GREEN);
        BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT OK", CENTER_MODE);
    }
    HAL_Delay(1000);
    BSP_LCD_Clear(LCD_COLOR_BLACK);
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
}
    

int main(void)
{
    uint8_t text[30];
    sprintf((char*)text, "CLEAR");
    initDisplay();
    initTS();
    //cannot pass two args, well played -> structure
    threadProperties threadProperties1 =
    { .ID = 1, .FLAG = SAMPLE_FLAG1 };
    threadProperties threadProperties2 =
    { .ID = 2, .FLAG = SAMPLE_FLAG2 };
    threadProperties threadProperties3 =
    { .ID = 3, .FLAG = SAMPLE_FLAG3 };
    threadProperties threadProperties4 =
    { .ID = 4, .FLAG = SAMPLE_FLAG4 };

    t1.start(callback(test_thread, &threadProperties1));
    t2.start(callback(test_thread, &threadProperties2));
    t3.start(callback(test_thread, &threadProperties3));
    t4.start(callback(test_thread, &threadProperties4));
    while (true) {
        registerTouch();
        if(thread_flags.get()==THREAD_DONE_FLAG)
        {
            BSP_LCD_ClearStringLine(STRING_LINE+1);
            thread_flags.clear(THREAD_DONE_FLAG);
        }
        if(!threadOperationRunning)
        {
            BSP_LCD_DisplayStringAt(0, LINE(STRING_LINE), (uint8_t *)&text, CENTER_MODE);
        }
        else{
            BSP_LCD_ClearStringLine(STRING_LINE);
        }
    }
}
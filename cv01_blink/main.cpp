#include "mbed.h"
#include <string>
#include <map> 

DigitalOut myled(LED1);

map<char, string> codingTable = {
    {'A',".-"},
    {'B',"-..."},
    {'C',"-.-."},
    {'D',"-.."},
    {'E',"."},
    {'F',"..-."},
    {'G',"--."},
    {'H',"...."},
    {'I',".."},
    {'J',".---"},
    {'K',"-.-"},
    {'L',".-.."},
    {'M',"--"},
    {'N',"-."},
    {'O',"---"},
    {'P',".--."},
    {'Q',"--.-"},
    {'R',".-."},
    {'S',"..."},
    {'T',"-"},
    {'U',"..-"},
    {'V',"...-"},
    {'W',".--"},
    {'X',"-..-"},
    {'Y',"-.--"},
    {'Z',"--.."}
};
//CHANGE ACCORDINGLY
#define DOT     200ms
#define DASH    3*DOT
string textToEncode = "Martin";

//FORWARD DECLARATIONS
void flash_morse_code(string encoded);
void flash_dot_or_dash(char dot_or_dash);
string build_morse_code(string encode);

int main() { 

    string encoded = build_morse_code(textToEncode);
    while(1) {
        flash_morse_code(encoded);
    }
}
string build_morse_code(string encode)
{
    string builder = "";
    for(int i = 0;i<encode.length();i++)
    {
        builder += codingTable[toupper(encode[i])];
    }
    return builder;
}
void flash_morse_code(string encoded) {
    
  unsigned int i = 0;
    
  while (encoded[i] != NULL) {
    flash_dot_or_dash(encoded[i]);
    i++;
  }  
  // Space between two letters is equal to three dots
  ThisThread::sleep_for(DASH);    
}
void flash_dot_or_dash(char dot_or_dash) {
   
  myled = 1;
   
  if (dot_or_dash == '.') {
    ThisThread::sleep_for(DOT);    
  }
  else {
    ThisThread::sleep_for(DASH);           
  }
  myled = 0;
  ThisThread::sleep_for(DOT); 
}

